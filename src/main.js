//const getTrafficHexColor = require('./traffic-lights');

// const { getTrafficHexColor, getNextTrafficState } = require('./src/traffic-lights');

// console.log(getTrafficHexColor("GREEN")); // should output "#00FF00"
// console.log(getNextTrafficState('RED')); // should output "YELLOW"

// 

const expect = require('chai').expect;
const { getTrafficHexColor, getNextTrafficState, getFlashYellowHexColor } = require('./src/traffic-lights');

describe('Traffic light functions', () => {
  describe('getTrafficHexColor', () => {
    it('Should return #00FF00 for GREEN', () => {
      expect(getTrafficHexColor('GREEN')).to.equal('#00FF00');
    });

    it('Should return #FFFF00 for YELLOW', () => {
      expect(getTrafficHexColor('YELLOW')).to.equal('#FFFF00');
    });

    it('Should return #FFFF00 for FLASH_YELLOW', () => {
      expect(getTrafficHexColor('FLASH_YELLOW')).to.equal('#FFFF00');
    });

    it('Should return #FF0000 for RED', () => {
      expect(getTrafficHexColor('RED')).to.equal('#FF0000');
    });

    it('Should return undefined for unknown color', () => {
      expect(getTrafficHexColor('BLUE')).to.be.undefined;
    });
  });

  describe('getNextTrafficState', () => {
    it('Should return YELLOW for RED', () => {
      expect(getNextTrafficState('RED')).to.equal('YELLOW');
    });

    it('Should return GREEN for YELLOW', () => {
      expect(getNextTrafficState('YELLOW')).to.equal('GREEN');
    });

    it('Should return FLASH_YELLOW for GREEN', () => {
      expect(getNextTrafficState('GREEN')).to.equal('FLASH_YELLOW');
    });

    it('Should return YELLOW for FLASH_YELLOW', () => {
      expect(getNextTrafficState('FLASH_YELLOW')).to.equal('YELLOW');
    });

    it('Should return RED for unknown state', () => {
      expect(getNextTrafficState('PURPLE')).to.equal('RED');
    });
  });

  describe('getFlashYellowHexColor', () => {
    it('Should return #FFFF00', () => {
      expect(getFlashYellowHexColor()).to.equal('#FFFF00');
    });
  });
});
