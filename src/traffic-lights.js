const getTrafficHexColor = (word) => {
    if (word === "GREEN") {
        return "#00FF00";
    } else if (word === "YELLOW" || word === "FLASH_YELLOW") {
        return "#FFFF00";
    } else if (word === "RED") {
        return "#FF0000";
    } else {
        return undefined;
    }
}

const getNextTrafficState = (currentState) => {
    if (currentState === 'RED') {
      return 'YELLOW';
    } else if (currentState === 'YELLOW') {
      return 'GREEN';
    } else if (currentState === 'GREEN') {
      return 'FLASH_YELLOW';
    } else if (currentState === 'FLASH_YELLOW') {
      return 'YELLOW';
    } else {
      return 'RED';
    }
};

const getFlashYellowHexColor = () => {
    return "#FFFF00";
};

module.exports = { getTrafficHexColor, getNextTrafficState, getFlashYellowHexColor };
