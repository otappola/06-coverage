const expect = require('chai').expect;
const { getTrafficHexColor, getNextTrafficState } = require('../src/traffic-lights');

describe("Traffic lights", () => {
    it("Test traffic lights possibilities", () => {
        expect(getTrafficHexColor("GREEN")).to.equal("#00FF00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#FFFF00");
        expect(getTrafficHexColor("RED")).to.equal("#FF0000");
        expect(getTrafficHexColor("blue")).to.equal(undefined);
    });
});

describe('Traffic light transitions', () => {
  it('Should return the correct next state for each current state', () => {
    expect(getNextTrafficState('RED')).to.equal('YELLOW');
    expect(getNextTrafficState('YELLOW')).to.equal('GREEN');
    expect(getNextTrafficState('GREEN')).to.equal('YELLOW');
    expect(getNextTrafficState('blue')).to.equal('RED');
  });
});
